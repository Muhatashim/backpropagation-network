package org.virus.bpn;

import org.virus.bpn.api.BPN;
import org.virus.bpn.api.Lesson;
import org.virus.bpn.api.Test;
import org.virus.bpn.api.transferfuncitons.TransferFunction;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 6/6/13
 * Time: 10:16 AM
 */
public class Main {

    public static void main(String[] args) {
        BPN bpn = new BPN(3, .25, 2, TransferFunction.SIGMOID);

        Lesson l1 = new Lesson(new int[]{0, 1}, 1);
        Lesson l2 = new Lesson(new int[]{1, 1}, 0);
        Lesson l3 = new Lesson(new int[]{1, 0}, 1);
        Lesson l4 = new Lesson(new int[]{0, 0}, 0);

        for (int i = 0; i < 100000; i++)
            System.out.println("Error " + bpn.teach(l1, l2, l3, l4));

        System.out.println("Answer: " + bpn.answer(new Test(new int[]{1, 0})));
        System.out.println("Answer: " + bpn.answer(new Test(new int[]{1, 1})));
        System.out.println("Answer: " + bpn.answer(new Test(new int[]{0, 1})));
        System.out.println("Answer: " + bpn.answer(new Test(new int[]{0, 0})));
    }
}
