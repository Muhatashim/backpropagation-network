package org.virus.bpn.api.nodes;

import org.virus.bpn.api.BPN;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 6/6/13
 * Time: 11:15 AM
 */
public class BiasNode extends Node {

    public BiasNode(BPN network) {
        super(network);
    }

    @Override
    public double input() {
        return 1;
    }
}
