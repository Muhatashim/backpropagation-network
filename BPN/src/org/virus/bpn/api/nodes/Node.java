package org.virus.bpn.api.nodes;

import org.virus.bpn.api.BPN;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 6/6/13
 * Time: 10:19 AM
 */
public class Node {

    private final BPN                   network;
    private       HashMap<Node, Double> parents;
    private       HashMap<Node, Double> children;
    private       double                output;

    public Node(BPN network) {
        this.network = network;
        output = -1;
    }

    public void init(HashMap<Node, Double> parents, HashMap<Node, Double> children) {
        this.parents = parents;
        this.children = children;
    }

    public double getParentWeight(Node parent) {
        return parents.get(parent);
    }

    public double getChildWeight(Node child) {
        return children.get(child);
    }

    public double input() {
        double input = 0;

        for (Node pNode : parents.keySet()) {
            input += pNode.calculateOutput() * pNode.getChildWeight(this);
        }

        return input;
    }

    public void updateWeights(double error) {
        for (Node cNode : children.keySet()) {
            double oldVal = getChildWeight(cNode);
            double c_output = cNode.calculateOutput();

            children.put(cNode, oldVal
                    + network.getLearningRate() * error * calculateOutput()
                    * c_output * (1 - c_output));
        }
    }

    public double findError() {
        return network.getLesson().getOutput() - calculateOutput();
    }

    public double calculateOutput() {
        return output = network.getTransferFunction().transfer(input());
    }

    @Override
    public String toString() {
        return "Node{" +
                "output=" + output +
                '}';
    }
}