package org.virus.bpn.api.nodes;

import org.virus.bpn.api.BPN;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 6/6/13
 * Time: 11:05 AM
 */
public class InputNode extends Node {

    private int input;

    public InputNode(BPN network) {
        super(network);
    }

    public void setInput(int input) {
        this.input = input;
    }

    @Override
    public double input() {
        return input;
    }
}
