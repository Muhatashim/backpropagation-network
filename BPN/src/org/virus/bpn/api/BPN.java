package org.virus.bpn.api;

import org.virus.bpn.api.nodes.BiasNode;
import org.virus.bpn.api.nodes.InputNode;
import org.virus.bpn.api.nodes.Node;
import org.virus.bpn.api.transferfuncitons.TransferFunction;

import java.util.HashMap;
import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 6/6/13
 * Time: 10:19 AM
 */
public class BPN {

    private final Node[][]         nodes;
    private final TransferFunction transferFunction;
    private final double           learningRate;
    private final int              inputs;
    private       Lesson           lesson;

    public BPN(int levels, double learningRate, int inputs, TransferFunction transferFunction) {
        this.learningRate = learningRate;
        this.inputs = inputs;
        nodes = new Node[levels][];
        this.transferFunction = transferFunction;

        fillNodes(inputs);
        setup();
    }

    public double getLearningRate() {
        return learningRate;
    }

    public Lesson getLesson() {
        return lesson;
    }

    public TransferFunction getTransferFunction() {
        return transferFunction;
    }

    public double answer(Test test) {
        int[] inputs = test.getInput();

        for (int i = 0; i < inputs.length; i++)
            ((InputNode) nodes[0][i]).setInput(inputs[i]);

        for (int i = 0; i < nodes.length - 1; i++)
            for (Node node : nodes[i])
                node.calculateOutput();

        return nodes[nodes.length - 1][0].calculateOutput();
    }

    public double teach(Lesson... lessons) {
        double error = 0;

        for (Lesson lesson : lessons)
            error = Math.pow(teach(lesson), 2);

        return error;
    }

    public double teach(Lesson lesson) {
        this.lesson = lesson;
        double error;
        int[] inputs = lesson.getInput();

        for (int i = 0; i < inputs.length; i++)
            ((InputNode) nodes[0][i]).setInput(inputs[i]);

        for (int i = 0; i < nodes.length; i++)
            for (Node node : nodes[i])
                node.calculateOutput();
        error = nodes[nodes.length - 1][0].findError();

        for (int i = nodes.length - 1; i >= 0; i--)
            for (Node node : nodes[i])
                node.updateWeights(error);

        return error;
    }

    private void fillNodes(int inputs) {
        for (int i = 0; i < nodes.length; i++) {
            nodes[i] = i == 0 ? new Node[inputs] : new Node[inputs + 1];

            if (i != 0)
                nodes[i][inputs] = new BiasNode(this);
            for (int i2 = 0; i2 < nodes[i].length; i2++) {
                if (i == 0)
                    nodes[i][i2] = new InputNode(this);
                else
                    nodes[i][i2] = new Node(this);
            }
        }

        nodes[nodes.length - 1] = new Node[]{new Node(this)};
    }

    public void setup() {
        for (int col = 0; col < nodes.length; col++)
            for (Node node : nodes[col]) {
                HashMap<Node, Double> parents = new HashMap<Node, Double>();
                HashMap<Node, Double> children = new HashMap<Node, Double>();
                if (col != 0)
                    for (Node pNode : nodes[col - 1])
                        parents.put(pNode, randomWeight());
                if (col != nodes.length - 1)
                    for (Node cNode : nodes[col + 1])
                        children.put(cNode, randomWeight());

                node.init(parents, children);
            }
    }

    private double randomWeight() {
        return new Random().nextDouble() * 2 - 1;
    }
}
