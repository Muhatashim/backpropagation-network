package org.virus.bpn.api;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 6/6/13
 * Time: 2:30 PM
 */
public class Test {

    private final int[] inputs;

    public Test(int[] inputs){
        this.inputs = inputs;
    }

    public int[] getInput() {
        return inputs;
    }
}
