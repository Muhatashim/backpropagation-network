package org.virus.bpn.api.transferfuncitons;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 6/6/13
 * Time: 10:46 AM
 */
public enum TransferFunction {
    SIGMOID {
        @Override
        public double transfer(double input) {
            return 1 / (1 + Math.exp(-input));
        }
    };

    public abstract double transfer(double input);
}
