package org.virus.bpn.api;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 6/6/13
 * Time: 12:13 PM
 */
public class Lesson {
    private final int[] input;
    private final int   output;

    public Lesson(int input[], int output) {
        this.input = input;
        this.output = output;
    }

    public int[] getInput() {
        return input;
    }

    public int getOutput() {
        return output;
    }

    @Override
    public String toString() {
        return "Lesson{" +
                "input=" + Arrays.toString(input) +
                ", output=" + output +
                '}';
    }
}
